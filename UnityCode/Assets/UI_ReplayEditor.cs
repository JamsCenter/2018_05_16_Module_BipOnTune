﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class UI_ReplayEditor : MonoBehaviour {
    public GetTimeFrom m_time;
    public BipOnTune m_bipOnTune;

    public Slider m_sliderSeconds;
    public Slider m_sliderMicroSecond;
    public Text m_secondBetweenDisplay;

    public UI_ReplayEditorBip[] m_uiBips;

    public float secondInterval;

    public float m_currentTime;
    public int m_keyFramesCount;
    IEnumerator Start() {
        while (true) { 
        yield return new WaitForSeconds(0.05f);
        secondInterval = m_sliderSeconds.value + m_sliderMicroSecond.value;
        float t = m_currentTime= m_time.GetTime();
        float min=t -secondInterval;
        float max=t + secondInterval;
        m_secondBetweenDisplay.text = ""+secondInterval;
        List<TuneKeyFrame>  frames =  m_bipOnTune.GetActionsToDo(min, max);
        m_keyFramesCount = frames.Count;
        for (int i = 0; i < m_uiBips.Length; i++)
        {
            bool on = i<frames.Count ;
            m_uiBips[i].gameObject.SetActive(on);
            if (on) {
                float pct = (frames[i].m_time-min)/(max-min);
                m_uiBips[i].SetPourcent(pct);
                m_uiBips[i].SetInfo(frames[i].m_descriptionName, frames[i].m_time);
                m_uiBips[i].SetFrame(frames[i]);

                m_uiBips[i].m_description.onValueChange.RemoveAllListeners();
                m_uiBips[i].m_timer.onValueChange.RemoveAllListeners();
                m_uiBips[i].m_description.onValueChange.AddListener(Changed(frames[i], m_uiBips[i]));
                m_uiBips[i].m_timer.onValueChange.AddListener(Changed(frames[i], m_uiBips[i]));

                m_uiBips[i].m_removeFrame.onClick.RemoveAllListeners();
                m_uiBips[i].m_removeFrame.onClick.AddListener(Remove(frames[i]));
            }
        }
        }
    }

    private UnityAction<string> Changed(TuneKeyFrame tuneKeyFrame, UI_ReplayEditorBip uI_ReplayEditorBip)
    {
        UnityAction<string> action = new UnityAction<string>(delegate (string text) {

            m_bipOnTune.ChangeValueOf(tuneKeyFrame, uI_ReplayEditorBip.m_description.text, uI_ReplayEditorBip.GetTimer());
            
        });
        return action;
    }

    private UnityAction Remove(TuneKeyFrame tuneKeyFrame)
    {
        UnityAction action = new UnityAction(delegate() {
            Debug.Log("Remove:" + tuneKeyFrame.m_descriptionName);
            m_bipOnTune.Remove(tuneKeyFrame);
        });
        return action;
    }
}
