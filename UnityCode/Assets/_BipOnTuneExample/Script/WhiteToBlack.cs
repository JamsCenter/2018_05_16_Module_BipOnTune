﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class WhiteToBlack : MonoBehaviour {

    public Camera m_camera;
    public Color m_startColor;
    public Color m_endColor;
    public float m_timeToDo = 0.1f;
    public float m_countDown;

    public void Pulse() {
        m_camera.backgroundColor = m_startColor;
        m_countDown = m_timeToDo;
    }

    public void Update()
    {
        if (m_countDown > 0f) {
            m_countDown -= Time.deltaTime;

            if (m_countDown < 0)
                m_countDown = 0;

            m_camera.backgroundColor = Color.Lerp(m_startColor, m_endColor, 1f- Mathf.Clamp01( m_countDown / m_timeToDo));

        }
    }

}
