﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public abstract class GetTimeFrom : MonoBehaviour {

    public abstract float GetTime();
}
