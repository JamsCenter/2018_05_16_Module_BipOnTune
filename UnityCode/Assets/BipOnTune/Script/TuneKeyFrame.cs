﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

[System.Serializable]
public class TuneKeyFrame
{

    public string m_descriptionName;
    public float m_time;
    public ToDoEvent m_toDo;


    [System.Serializable]
    public class ToDoEvent : UnityEvent<float, string>
    {
     
    }

    internal void DoTheThing()
    {
        if(m_toDo!=null)
            m_toDo.Invoke(m_time, m_descriptionName);
    }

    internal TuneKeyFrame GetCopy()
    {
        TuneKeyFrame copy = new TuneKeyFrame();
        copy.m_time = this.m_time;
        copy.m_descriptionName = this.m_descriptionName;
        copy.m_toDo = this.m_toDo;
        return copy;
    }
}


[System.Serializable]
public class TrackActions
{
    public string m_name="Unnamed";
    public List<TuneKeyFrame> m_keyframes = new List<TuneKeyFrame>();

    internal void Add(TuneKeyFrame keyframe)
    {
        m_keyframes.Add(keyframe);
    }

    internal void Clear()
    {
        m_keyframes.Clear();
    }
}


