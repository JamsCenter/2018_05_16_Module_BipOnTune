﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEditor;

public class BipOnTuneFrameToAudioSource : MonoBehaviour {

    public AudioSource m_audioSource;

	// Use this for initialization
	public void Play (float time, string description) {

        Debug.Log("Play: "+description+"!!!", this.gameObject);
        if(description =="D")
        EditorApplication.Beep();
        m_audioSource.Play();

    }

    // Update is called once per frame
    public void Stop(float time, string description)
    {
        Debug.Log("Stop :(", this.gameObject);
        m_audioSource.Stop();

    }
}
